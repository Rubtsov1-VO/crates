tee $DOCKERFILE <<-EOL
FROM node:10
RUN groupadd --gid 1001 app \
  && useradd --uid 1001 --gid 1001 --shell /bin/bash --create-home --home /home/app app
COPY --chown=app:app code /code
USER app
RUN cd ${WORKDIR} && npm run setup
RUN mkdir -p /code/web/public/js/bundles && chown -R app:app /code/web/public/js/bundles \
    && chown -R app:app /home/app/.config
WORKDIR ${WORKDIR}
EXPOSE ${EXPOSE_PORT}
ENTRYPOINT ["npm", "start"]
EOL
